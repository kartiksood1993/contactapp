package com.pro.kartik.contactapp.persistence.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pro.kartik.contactapp.persistence.Contact

@Database(entities = [Contact::class], version = 1, exportSchema = false)
abstract class ContactRoomDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}