package com.pro.kartik.contactapp.persistence.cache

import android.content.Context
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

class PicassoClient constructor(val context: Context) {
    fun createPicassoClient(): Picasso {
        val cache = okhttp3.Cache(context.cacheDir, (10 * 1024 * 1024).toLong())
        val httpClient = HttpClient.createOkHttpClientBuilder(cache).build()
        return Picasso.Builder(context).downloader(OkHttp3Downloader(httpClient)).build()
    }
}