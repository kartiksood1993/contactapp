package com.pro.kartik.contactapp.dagger

import androidx.room.Room
import com.pro.kartik.contactapp.persistence.cache.PicassoClient
import com.pro.kartik.contactapp.application.ContactApplication
import com.pro.kartik.contactapp.persistence.Repository
import com.pro.kartik.contactapp.persistence.local.ContactDao
import com.pro.kartik.contactapp.persistence.local.ContactRoomDatabase
import com.pro.kartik.contactapp.utils.Constants
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: ContactApplication) {
    @Provides
    @ApplicationScope
    fun providesApplication(): ContactApplication {
        return application
    }

    @Provides
    @ApplicationScope
    fun provideContactDao(contactRoomDatabase: ContactRoomDatabase): ContactDao {
        return contactRoomDatabase.contactDao()
    }

    @Provides
    @ApplicationScope
    fun provideContactDatabase(): ContactRoomDatabase {
        return Room.databaseBuilder(
            application,
            ContactRoomDatabase::class.java,
            Constants.DATABASE_NAME
        ).build()
    }

    @Provides
    @ApplicationScope
    fun provideContactRepository(contactDao: ContactDao): Repository {
        return Repository(contactDao)
    }

    @Provides
    @ApplicationScope
    fun providePicasso(): Picasso {
        return PicassoClient(application).createPicassoClient()
    }
}