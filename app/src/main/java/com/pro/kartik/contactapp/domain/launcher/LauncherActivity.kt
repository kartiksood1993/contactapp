package com.pro.kartik.contactapp.domain.launcher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pro.kartik.contactapp.R
import com.pro.kartik.contactapp.domain.contactsList.ContactsListActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LauncherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        CoroutineScope(Dispatchers.Main).launch {
            delay(2000)
            ContactsListActivity.startActivity(this@LauncherActivity)
            finish()
        }
    }
}