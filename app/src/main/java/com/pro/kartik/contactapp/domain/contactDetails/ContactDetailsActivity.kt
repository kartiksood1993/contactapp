package com.pro.kartik.contactapp.domain.contactDetails

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProvider
import com.pro.kartik.contactapp.R
import com.pro.kartik.contactapp.application.ContactApplication
import com.pro.kartik.contactapp.databinding.ActivityContactDetailsBinding
import com.pro.kartik.contactapp.domain.contactsList.ContactsListActivity
import com.pro.kartik.contactapp.persistence.Contact
import com.pro.kartik.contactapp.receivers.UpdateContactReceiver
import com.pro.kartik.contactapp.utils.Constants
import com.pro.kartik.contactapp.utils.Constants.BundleParams.ARG_CONTACT_DATA
import com.pro.kartik.contactapp.utils.Constants.BundleParams.ARG_DELETE_CONTACT
import com.pro.kartik.contactapp.utils.Constants.BundleParams.ARG_UPDATE_CONTACT
import com.squareup.picasso.Picasso
import java.io.IOException
import javax.inject.Inject

class ContactDetailsActivity : AppCompatActivity() {
    companion object {
        fun startActivity(activity: AppCompatActivity, contact: Contact) {
            val intent = Intent(activity, ContactDetailsActivity::class.java)
            intent.putExtra(ARG_CONTACT_DATA, contact)
            activity.startActivityForResult(intent, ARG_UPDATE_CONTACT)
        }
    }

    private lateinit var contactDetailsViewModel: ContactDetailsViewModel
    private lateinit var contactDetailsBinding: ActivityContactDetailsBinding
    private lateinit var contactDetails: Contact

    @Inject
    lateinit var picasso: Picasso

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ContactApplication.getApplicationComponent()?.inject(this)
        contactDetailsBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_contact_details
        )
        contactDetails = intent.extras?.getSerializable(ARG_CONTACT_DATA) as Contact
        setUpViewModel()
        setProfileImage()
        if (!hasContactReadPermission()) {
            requestPermission()
        }
    }

    private fun setUpViewModel() {
        contactDetailsViewModel =
            ViewModelProvider(
                this,
                ContactDetailsViewModelFactory(
                    ContactApplication.getApplicationComponent()?.getContactRepositoryInstance()!!,
                    ContactApplication.getApplicationComponent()?.getApplicationInstance()!!,
                    contactDetails
                )
            ).get(
                ContactDetailsViewModel::class.java
            )
        contactDetailsBinding.viewModel = contactDetailsViewModel
        contactDetailsViewModel.showContactInformation()
        contactDetailsViewModel.contactUpdate.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (contactDetailsViewModel.contactUpdate.get()) {
                    Toast.makeText(
                        this@ContactDetailsActivity,
                        getString(R.string.contact_details_updated),
                        Toast.LENGTH_SHORT
                    ).show()
                    updateAndDeleteContactDetailsAndGetBackToPreviousActivity(ARG_UPDATE_CONTACT)
                } else {
                    Toast.makeText(
                        this@ContactDetailsActivity, getString(R.string.unable_to_update_contact), Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })

        contactDetailsViewModel.contactDelete.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (contactDetailsViewModel.contactDelete.get()) {
                    Toast.makeText(
                        this@ContactDetailsActivity,
                        getString(R.string.contact_deleted_success),
                        Toast.LENGTH_SHORT
                    ).show()
                    updateAndDeleteContactDetailsAndGetBackToPreviousActivity(ARG_DELETE_CONTACT)
                } else {
                    Toast.makeText(
                        this@ContactDetailsActivity,
                        getString(R.string.unable_to_delete_contact),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun setProfileImage() {
        contactDetails.profilePictureUrl?.let {
            try {
                val uri = Uri.parse(it)
                picasso.load(uri).into(contactDetailsBinding.contactProfileImage)
            } catch (e: IOException) {
                e.printStackTrace()
                picasso.load(R.drawable.avatar_icon)
                    .into(contactDetailsBinding.contactProfileImage)
            }
        }
    }

    private fun hasContactReadPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.WRITE_CONTACTS),
                Constants.Permissions.WRITE_CONTACT_PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == Constants.Permissions.WRITE_CONTACT_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                requestPermission()
            }
        }
    }

    private fun updateAndDeleteContactDetailsAndGetBackToPreviousActivity(resultCode: Int) {
        contactDetails.name = contactDetailsViewModel.name.get()
        contactDetails.number = contactDetailsViewModel.phoneNumber.get()
        contactDetails.email = contactDetailsViewModel.email.get()
        if (resultCode == ARG_UPDATE_CONTACT) {
            sendBroadcast(Intent(this, UpdateContactReceiver::class.java))
        }
        val intent =
            Intent(this@ContactDetailsActivity, ContactsListActivity::class.java)
        intent.putExtra(ARG_CONTACT_DATA, contactDetails)
        setResult(resultCode, intent)
        finish()
    }
}