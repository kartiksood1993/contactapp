package com.pro.kartik.contactapp.persistence.cache

import okhttp3.OkHttpClient

object HttpClient {
    fun createOkHttpClientBuilder(cache: okhttp3.Cache?): OkHttpClient.Builder {
        return OkHttpClient.Builder().cache(cache)
    }
}