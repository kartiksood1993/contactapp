package com.pro.kartik.contactapp.application

import android.app.Application
import com.pro.kartik.contactapp.dagger.ApplicationComponent
import com.pro.kartik.contactapp.dagger.ApplicationModule
import com.pro.kartik.contactapp.dagger.DaggerApplicationComponent

class ContactApplication : Application() {
    companion object {
        private var applicationComponent: ApplicationComponent? = null

        fun getApplicationComponent(): ApplicationComponent? {
            return applicationComponent
        }

        fun setApplicationComponent(applicationComponent: ApplicationComponent?) {
            this.applicationComponent = applicationComponent
        }
    }

    override fun onCreate() {
        super.onCreate()

        setApplicationComponent(
            DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        )
    }
}