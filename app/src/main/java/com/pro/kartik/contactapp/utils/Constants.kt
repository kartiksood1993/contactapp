package com.pro.kartik.contactapp.utils

object Constants {
    const val DATABASE_NAME = "contacts_list_db"
    const val sEmpty_String = ""

    object Permissions {
        const val READ_CONTACT_PERMISSION_REQUEST_CODE = 100
        const val WRITE_CONTACT_PERMISSION_REQUEST_CODE = 101
    }

    object BundleParams{
        const val ARG_CONTACT_DATA = "contactItem"
        const val ARG_UPDATE_CONTACT = 100
        const val ARG_DELETE_CONTACT = 101
    }
}