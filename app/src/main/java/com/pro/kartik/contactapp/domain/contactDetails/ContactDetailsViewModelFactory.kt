package com.pro.kartik.contactapp.domain.contactDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pro.kartik.contactapp.application.ContactApplication
import com.pro.kartik.contactapp.persistence.Contact
import com.pro.kartik.contactapp.persistence.Repository

@Suppress("UNCHECKED_CAST")
class ContactDetailsViewModelFactory(
    private val repository: Repository,
    private val application: ContactApplication,
    private val contact: Contact
) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ContactDetailsViewModel(repository, application, contact) as T
    }
}