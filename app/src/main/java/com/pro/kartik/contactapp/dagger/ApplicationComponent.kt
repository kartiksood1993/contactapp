package com.pro.kartik.contactapp.dagger

import com.pro.kartik.contactapp.application.ContactApplication
import com.pro.kartik.contactapp.domain.contactDetails.ContactDetailsActivity
import com.pro.kartik.contactapp.domain.contactsList.ContactsListActivity
import com.pro.kartik.contactapp.persistence.Repository
import dagger.Component

@Component(modules = [ApplicationModule::class])
@ApplicationScope
interface ApplicationComponent {
    fun getApplicationInstance(): ContactApplication
    fun getContactRepositoryInstance(): Repository

    fun inject(contactDetailsActivity: ContactDetailsActivity)
}