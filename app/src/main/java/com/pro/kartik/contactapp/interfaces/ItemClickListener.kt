package com.pro.kartik.contactapp.interfaces

interface ItemClickListener{
    fun onItemClick(position: Int)
}